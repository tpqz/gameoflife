#-------------------------------------------------
#
# Project created by QtCreator 2016-10-21T16:01:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GameOfLife
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cell.cpp \
    grid.cpp

HEADERS  += mainwindow.h \
    grid.h \
    cell.h

FORMS    += mainwindow.ui

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "cell.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	Ui::MainWindow *ui;

private slots:
	void on_exitButton_clicked();

protected:

};

#endif // MAINWINDOW_H

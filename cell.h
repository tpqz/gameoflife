#ifndef CELL_H
#define CELL_H

#include <QGraphicsRectItem>
#include <QList>

class Cell : public QObject, public QGraphicsRectItem {
	Q_OBJECT

private:

	int SIZE;

	QList<Cell *> aliveNeighbours;
	QList<Cell *> deadNeighbours;
	bool alive;

//	int aliveNeighbours;
//	int deadNeighbours;


protected:


public:
	Cell(QGraphicsItem * parent = NULL);
	QList<QGraphicsLineItem*> lines;
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void setAlive(bool alive);
	//bool getAlive(Cell * cell);
	void createLines();
	void findNeighbours();
	void checkNeighbourState();
	bool getAlive();

public slots:
	void setSize(int size);
};

#endif // CELL_H

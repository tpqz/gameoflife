#include "cell.h"
#include "grid.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QBrush>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QGraphicsTextItem>

int CELL_SIZE = 100; //size of one cell

Cell::Cell(QGraphicsItem *parent) : QGraphicsRectItem(parent){
	setAlive(false);

//	aliveNeighbours = 0;
//	deadNeighbours = 0;

	SIZE = CELL_SIZE; //set size of one cell
	setSize(SIZE); //call function setSize to apply size changes
	setRect(0,0,SIZE,SIZE);
	//connect(MainWindow::ui->sizeSlider,&QSlider::valueChanged, this, &Cell::setSize);

//	QBrush brush;
//	brush.setColor(Qt::black); //set color of one dead cell
//	brush.setStyle(Qt::SolidPattern); //set pattern of cell
//	setBrush(brush); //set changes to the cell

	//createLines();

//	qDebug() << aliveNeighbours;
//	qDebug() << deadNeighbours;
}

void Cell::setAlive(bool alive){
	//if cell is dead set alive && if cell is alive set dead
	QBrush brush;
	brush.setColor(Qt::green);
	brush.setStyle(Qt::SolidPattern);

	if (alive){
		setBrush(brush);
		this->alive = true;
	}else{
		brush.setColor(Qt::black);
		setBrush(brush);
		this->alive = false;
	}
}

void Cell::createLines(){
	QPointF hexCenter(x()+CELL_SIZE/2,y()+CELL_SIZE/2);
	QPointF finalPoint(hexCenter.x(),hexCenter.y()+CELL_SIZE-CELL_SIZE/4);
	QPointF finalPoint2(hexCenter.x(),hexCenter.y()+CELL_SIZE);
	QLineF ln(hexCenter,finalPoint);
	QLineF ln2(hexCenter,finalPoint2);

for(int i = 0; i < 8; i++){
	if (i % 2 == 0){
		QLineF lnCopy(ln);
		lnCopy.setAngle(45*i);
		QGraphicsLineItem * line = new QGraphicsLineItem(lnCopy,this);
		lines.append(line);
		line->setVisible(false);
	}else{
		QLineF lnCopy(ln2);
		lnCopy.setAngle(45*i);
		QGraphicsLineItem * line = new QGraphicsLineItem(lnCopy,this);
		lines.append(line);
		line->setVisible(false);
	}
}
}


void Cell::checkNeighbourState(){
	for(int i = 0; i < lines.size(); i++){
		//!TODO!
	}
}

bool Cell::getAlive()
{
	return alive;
}


void Cell::setSize(int size){
	SIZE = size;
}


void Cell::mousePressEvent(QGraphicsSceneMouseEvent *event){
	if (event->button() == Qt::RightButton){
		setAlive(false);
	}else if(event->button() == Qt::LeftButton){
		setAlive(true);
	}
}



//void Grid::findNeighbours(){
//	for (int i = 0; i < ; i++){
//		//if the line collides with an item of type Cell check if is dead or alive
//		QList<QGraphicsItem*> cItems = lines[i]->collidingItems();
//		qDebug() << "colliding items: ";
//		qDebug() << cItems.size();
//		qDebug() << "line number: ";
//		qDebug() << i;

//		for(int j = 0; j < cItems.size(); j++){
//			Cell * item = dynamic_cast<Cell *>(cItems[j]);

//			if (cItems[j] != this && item->alive == true){
//				qDebug() << "detected alive cell";
//				aliveNeighbours.append(item);
//			}else if(cItems[j] != this && item->alive == false){
//				//qDebug() << "detected dead cell";
//				deadNeighbours.append(item);
//				//qDebug() << deadNeighbours.size();
//			}
//		}
//	}
//	qDebug() << deadNeighbours.size();
//}

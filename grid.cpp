#include "grid.h"
#include "cell.h"

#include <QApplication>
#include <QDebug>
#include <QRect>
#include <QDesktopWidget>
#include <stdlib.h> //srand, rand
#include <time.h>
#include <QTimer>

extern int CELL_SIZE;
//int cellGridHeight = 0;
//int cellGridWidth = 0;
double SCALE_FACTOR = 1.15;

Grid::Grid(QWidget * /*parent*/){


	scene = new QGraphicsScene(this);
	rowsInCollumn = 0;
	yPos = 0;
	xPos = 0;

	QBrush brush;
	brush.setColor(Qt::black);
	brush.setStyle(Qt::SolidPattern);

	setBackgroundBrush(brush);

	//get screen resolution
	QRect rec = QApplication::desktop()->screenGeometry();
	int height = rec.height();
	int width = rec.width();

	//set scrollbarpolicyna
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	//create scene
	setSceneRect(-1,0,width-width/7, height-height/8+50);
	setFixedSize(width-width/7, height-height/8+50);
	setAlignment(Qt::AlignBaseline);
	setScene(scene);

	//create grid
	//createGrid(CELL_SIZE/3,CELL_SIZE/3,11,11);
	createGrid(0,0,230,400); //230,400
}

/*
 *
 *
 *
 * TODO:
 * CREATE NEIGHOBURS DETECTOR WORKING FINE
 * CREATE COPY OF EXISTING CELL
 * CREATE ARRAY OF CELLS IN NEXT GENERATION
 *
 *
 *
 *
*/

void Grid::createCellColumn(int x, int y, int rows){
	QRect rec = QApplication::desktop()->screenGeometry();
	int height = rec.height();

	rowsInCollumn = rows;

	for(int i = 0; i <= rows; i++){
		int randNumber = rand() % 3 + 1; // 1 - 2

		Cell * cell = new Cell();
		cell->setPos(x,y+(CELL_SIZE + 0)*i); //HERE CHANGE SPACE BETWEEN CELLS


		//if cell is in area of window then create it, else dont create next cell & break function
		if(cell->pos().y() < height-height/8+50 - CELL_SIZE){
			//make cell alive or dead by random
			if(randNumber % 2 != 0){
				cell->setAlive(false);
				//cell->setPos(x,y+(CELL_SIZE + 1)*i);
				scene->addItem(cell);
				cellsOnBoard.append(cell);
			}else if(randNumber % 2 == 0){
				cell->setAlive(true);
				//cell->setPos(x,y+(CELL_SIZE + 1)*i);
				scene->addItem(cell);
				cellsOnBoard.append(cell);
		}else{
			return;
		}
	}
	}
}

void Grid::createGrid(int x, int y, int rows, int cols){
	QRect rec = QApplication::desktop()->screenGeometry();
	int width = rec.width();

	for(int i = 0; i <= cols; i++){
		//if cell is in area of window then create it, else dont create next cell & break function
		if(x+CELL_SIZE*i < width-width/7 - CELL_SIZE){
			createCellColumn(x+(CELL_SIZE + 0)*i,y,rows); //HERE CHANGE SPACE BETWEEN CELLS
		}else{
			return;
		}
	}

}

void Grid::paintGrid(){

QRect rec = QApplication::desktop()->screenGeometry();
int height = rec.height();

	for(int i = 0; i < cellsOnBoard.size(); i++){
		int randNumber = rand() % 3 + 1; // 1 - 2

		//if cell is in area of window then create it, else dont create next cell & break function
		if(cellsOnBoard[i]->pos().y() < height-height/8 - CELL_SIZE){
			//make cell alive or dead by random
			if(randNumber % 2 != 0){
				//cellsOnBoard[i]->setAlive(false);
				findNeighbours(cellsOnBoard[i]);

			}else if(randNumber % 2 == 0){
				//cellsOnBoard[i]->setAlive(true);
				findNeighbours(cellsOnBoard[i]);
			}else{
				return;
		}
		}
	}
}


void Grid::findNeighbours(Cell *cell){
		aliveNeighbours = 0;
		deadNeighbours = 0;

		QList<QGraphicsItem*> cItems = cell->collidingItems();
		//bool isAlive = cell->getAlive();
		for(int i = 0; i < cItems.size(); i++){
			cell = dynamic_cast<Cell*>(cItems[i]);
				if (cell && cell->getAlive()){
					aliveNeighbours++;
				}else{
					deadNeighbours++;
				}

		}

		if(cell->getAlive() && aliveNeighbours != 2 && aliveNeighbours != 3)
			cell->setAlive(false);
		else if(cell->getAlive() == false && aliveNeighbours != 3)
			cell->setAlive(false);
		else
			cell->setAlive(true);


		//qDebug() << cell->getNumber();

		qDebug() << cItems.size();
		qDebug() << "alive" << aliveNeighbours;
		qDebug() << "dead" << deadNeighbours;

		aliveNeighbours = 0;
		deadNeighbours = 0;
}


//void Grid::paintGrid(){
//	QRect rec = QApplication::desktop()->screenGeometry();
//	int width = rec.width();

//	for(int i = 0; i < cellsOnBoard.size(); i++){
//		//if cell is in area of window then create it, else dont create next cell & break function
//		if(cellsOnBoard[i]->pos().x() < width-width/7 - CELL_SIZE){
//			paintCellCollumn();
//		}else{
//			break;
//		}
//	}
//}

void Grid::setZoomScale(int scale){

	SCALE_FACTOR = (double)scale/10;

}


void Grid::clearScene(){
	scene->clear();
	setSize(20);
	//CELL_SIZE = 20;
	cellsOnBoard.clear();
	createGrid(CELL_SIZE/3,CELL_SIZE/3,100,100);
	//createGrid(0,0,1000,1000);

	/*
	QRect rec = QApplication::desktop()->screenGeometry();
	int height = rec.height();
	int width = rec.width();

	//setSize(50);
	createGrid((width-width/7 - cellGridWidth+10), (height-height/8 - cellGridHeight+10),500,500);
	//createGrid((width-width/7 - 1160), (height-height/8 - 670),500,500);
	*/
}

void Grid::setSize(int size){
	//function connected with a button for scaling a cells
	CELL_SIZE = size;
}

void Grid::resize(){
	scene->clear();
	cellsOnBoard.clear();
	setSize(CELL_SIZE);
	//createGrid(CELL_SIZE/3,CELL_SIZE/3,200,300);
	createGrid(0,0,1000,1000);
//QTimer * timer = new QTimer();
//connect(timer, SIGNAL(timeout()), this, SLOT(paintGrid()));
//timer->start(100);

}

//void Grid::findNeighbours(){
//	for (int i = 0; i < ; i++){
//		//if the line collides with an item of type Cell check if is dead or alive
//		QList<QGraphicsItem*> cItems = lines[i]->collidingItems();
//		qDebug() << "colliding items: ";
//		qDebug() << cItems.size();
//		qDebug() << "line number: ";
//		qDebug() << i;

//		for(int j = 0; j < cItems.size(); j++){
//			Cell * item = dynamic_cast<Cell *>(cItems[j]);

//			if (cItems[j] != this && item->alive == true){
//				qDebug() << "detected alive cell";
//				aliveNeighbours.append(item);
//			}else if(cItems[j] != this && item->alive == false){
//				//qDebug() << "detected dead cell";
//				deadNeighbours.append(item);
//				//qDebug() << deadNeighbours.size();
//			}
//		}
//	}
//	qDebug() << deadNeighbours.size();
//}

#ifndef GRID_H
#define GRID_H

#include "cell.h"

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QList>
#include <QVector>
#include <QWheelEvent>

class Grid : public QGraphicsView {
	Q_OBJECT
private:
	QList <Cell*> cellsOnBoard;
	QList <Cell*> nextGenerationList;
	//QList<Cell *> aliveNeighbours;
	//QList<Cell *> deadNeighbours;

	int aliveNeighbours;
	int deadNeighbours;

	int rowsInCollumn;
	int yPos;
	int xPos;
//	int cellGridHeight;
//	int cellGridWidth;

public:
	Grid(QWidget * parent = 0);
	QGraphicsScene * scene;
	void createCellColumn(int x, int y, int rows);
	void createGrid(int x, int y, int rows, int cols);
	void findNeighbours(Cell *cell);

public slots:
	void paintGrid();
	void setZoomScale(int scale);
	void clearScene();
	void setSize(int size);
	void resize();
	//void nextGeneration(Cell * cell);
};

#endif // GRID_H

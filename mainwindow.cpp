#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "grid.h"
#include "cell.h"

#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow){

	ui->setupUi(this);
	QMainWindow::showFullScreen();
	connect(ui->resetButton,SIGNAL(clicked()), ui->graphicsView,SLOT(clearScene()));
	connect(ui->sizeSlider,SIGNAL(valueChanged(int)), ui->graphicsView,SLOT(setSize(int)));
	connect(ui->sizeSlider,SIGNAL(valueChanged(int)), ui->sizeScreen,SLOT(display(int)));
	connect(ui->resizeButton,SIGNAL(clicked(bool)), ui->graphicsView,SLOT(resize()));
	connect(ui->generationButton,SIGNAL(clicked(bool)), ui->graphicsView,SLOT(paintGrid()));
	connect(ui->zoomSlider,SIGNAL(valueChanged(int)), ui->graphicsView,SLOT(setZoomScale(int)));
	//connect(ui->resizeButton,SIGNAL(clicked(bool)), ui->resizeButton,SLOT(setEnabled(bool)));
}


MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_exitButton_clicked()
{
	close();
}

